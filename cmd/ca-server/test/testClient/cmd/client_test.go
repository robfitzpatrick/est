package client_test

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/base64"
	"gitlab.com/robfitzpatrick/est/cmd/ca-server/test/testClient/models"
	"gitlab.com/robfitzpatrick/est/cmd/ca-server/test/testClient/client"
	"gitlab.com/robfitzpatrick/est/cmd/ca-server/test/testClient/client/operations"
	"log"
	"net"
	"testing"
)

func TestGood(t *testing.T) {
	c := client.NewHTTPClientWithConfig(nil, &client.TransportConfig{Host: "localhost:8888", BasePath: "/v1", Schemes: nil})

	csrParams := operations.NewPostSignCsrParams()

	privKey, generateErr := rsa.GenerateKey(rand.Reader, 2048)
	if nil != generateErr {
		log.Fatalf("generateErr: %v\n", generateErr.Error())
	}

	template := x509.CertificateRequest{
		Subject: pkix.Name{
			CommonName:   "user ID 42",
			Organization: []string{"PiCam Co"},
		},
		SignatureAlgorithm: x509.SHA512WithRSA,
		DNSNames:           []string{"test.example.com"},
		IPAddresses:        []net.IP{net.IPv4(127, 0, 0, 1).To4(), net.ParseIP("192.168.178.43")},

	}

	csr, csrErr := x509.CreateCertificateRequest(rand.Reader, &template, privKey)
	if nil != csrErr {
		log.Fatalf("csrErr: %v\n", csrErr.Error())
	}

	b64Csr := base64.StdEncoding.EncodeToString(csr)

	csrParams.Csr = &models.Csr{CsrBodyB64: &b64Csr}

	signCsrOK, signCsrErr := c.Operations.PostSignCsr(csrParams)
	if nil != signCsrErr {
		log.Fatalf("signCsrErr: %v\n", signCsrErr.Error())
	}

	providedCert := *(signCsrOK.GetPayload().CertificateBodyB64)
	decodedCert, _ := base64.StdEncoding.DecodeString(providedCert)

	parsedCert, parseErr := x509.ParseCertificate(decodedCert)
	if nil != parseErr {
		log.Fatalf("%v\n", parseErr.Error())
	}

	getCaCertParams := operations.NewGetCaCertParams()
	getCaCertOk, getCaCertErr := c.Operations.GetCaCert(getCaCertParams)
	if nil != getCaCertErr {
		log.Fatalf("getCaCertErr: %v\n", getCaCertErr.Error())
	}

	caCertDecoded, b64Err := base64.StdEncoding.DecodeString(*(getCaCertOk.Payload.CertificateBodyB64))
	if nil != b64Err {
		log.Fatalf("b64Err: %v\n", b64Err.Error())
	}
	caCert, caCertErr := x509.ParseCertificate(caCertDecoded)
	if nil != caCertErr {
		log.Fatalf("caCertErr: %v\n", caCertErr.Error())
	}


	//And validate our new one
	checkErr := parsedCert.CheckSignatureFrom(caCert)
	if nil != checkErr {
		log.Fatalf("checkErr: %v\n", checkErr.Error())
	}


}



func TestBad(t *testing.T) {
	c := client.NewHTTPClientWithConfig(nil, &client.TransportConfig{Host: "localhost:8888", BasePath: "/v1", Schemes: nil})

	csrParams := operations.NewPostSignCsrParams()

	privKey, generateErr := rsa.GenerateKey(rand.Reader, 2048)
	if nil != generateErr {
		log.Fatalf("generateErr: %v\n", generateErr.Error())
	}

	template := x509.CertificateRequest{
		Subject: pkix.Name{
			CommonName:   "user ID 42",
			Organization: []string{"PiCam Co"},
		},
		SignatureAlgorithm: x509.SHA512WithRSA,
		DNSNames:           []string{"test.example.com"},
		IPAddresses:        []net.IP{net.IPv4(127, 0, 0, 1).To4(), net.ParseIP("192.168.178.43")},

	}

	csr, csrErr := x509.CreateCertificateRequest(rand.Reader, &template, privKey)
	if nil != csrErr {
		log.Fatalf("csrErr: %v\n", csrErr.Error())
	}

	b64Csr := base64.StdEncoding.EncodeToString(csr)

	csrParams.Csr = &models.Csr{CsrBodyB64: &b64Csr}

	signCsrOK, signCsrErr := c.Operations.PostSignCsr(csrParams)
	if nil != signCsrErr {
		log.Fatalf("signCsrErr: %v\n", signCsrErr.Error())
	}

	providedCert := *(signCsrOK.GetPayload().CertificateBodyB64)
	decodedCert, _ := base64.StdEncoding.DecodeString(providedCert)

	parsedCert, parseErr := x509.ParseCertificate(decodedCert)
	if nil != parseErr {
		log.Fatalf("%v\n", parseErr.Error())
	}

	getCaCertParams := operations.NewGetCaCertParams()
	getCaCertOk, getCaCertErr := c.Operations.GetCaCert(getCaCertParams)
	if nil != getCaCertErr {
		log.Fatalf("getCaCertErr: %v\n", getCaCertErr.Error())
	}

	caCertDecoded, b64Err := base64.StdEncoding.DecodeString(*(getCaCertOk.Payload.CertificateBodyB64))
	if nil != b64Err {
		log.Fatalf("b64Err: %v\n", b64Err.Error())
	}
	_, caCertErr := x509.ParseCertificate(caCertDecoded)
	if nil != caCertErr {
		log.Fatalf("caCertErr: %v\n", caCertErr.Error())
	}

	//And try to self-validate
	checkErr := parsedCert.CheckSignatureFrom(parsedCert)
	if nil == checkErr {
		t.Fatalf("Expected a verification fail\n")
	}


}



