package ca_server

import (
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/base64"
	"github.com/ThalesIgnite/crypto11"
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/robfitzpatrick/est/cmd/ca-server/models"
	"gitlab.com/robfitzpatrick/est/cmd/ca-server/restapi/operations"
	"log"
	"math/big"
	"time"
)

var globalCert *x509.Certificate
var caPrivKey crypto11.SignerDecrypter

func SetGlobalCert(certBytes []byte) {
	var certErr error
	globalCert, certErr = x509.ParseCertificate(certBytes)
	if nil != certErr {
		log.Fatalf("certErr: %v\n", certErr.Error())
	}
}

func GenerateCaSigningKey(ctx *crypto11.Context) crypto11.Signer {

	id := []byte{0x00, 0x01, 0x02, 0x03}
	label := []byte("piCam CA key")

	key, err := ctx.GenerateRSAKeyPairWithLabel(id, label, 4096)
	if nil != err {
		log.Fatalf("err: %v\n", err.Error())
	}
	return key
}

func FindCaSigningKey(ctx *crypto11.Context) crypto11.Signer {


	id := []byte{0x00, 0x01, 0x02, 0x03}
	label := []byte("piCam CA key")

	// Do we already have this key?
	foundKey, findKeyErr := ctx.FindKeyPair(id, label)
	if nil != findKeyErr {
		log.Fatalf("findKeyErr: %v\n", findKeyErr.Error())
	}

	return foundKey
}

func GetCaCertHandler(params operations.GetCaCertParams) middleware.Responder {

	b64Cert := base64.StdEncoding.EncodeToString(globalCert.Raw)

	returnedCert := models.Certificate{CertificateBodyB64: &b64Cert}

	return operations.NewGetCaCertOK().WithPayload(&returnedCert)
}

func PostSignCsrHandler(params operations.PostSignCsrParams) middleware.Responder {

	decodedCSR, decodeErr := base64.StdEncoding.DecodeString(*params.Csr.CsrBodyB64)
	if nil != decodeErr {
		log.Printf("decodeErr: %v\n", decodeErr.Error())
		return operations.NewPostSignCsrBadRequest()
	}

    csr, csrParseError := x509.ParseCertificateRequest(decodedCSR)
    if nil != csrParseError {
    	log.Printf("csrParseError: %v\n", csrParseError.Error())
    	return operations.NewPostSignCsrBadRequest()
	}

    sigErr := csr.CheckSignature()
	if nil != sigErr {
		log.Printf("Signature failed verification: %v\n", sigErr.Error())
		code := int32(400)
		message := "Bad signature"
		return operations.NewPostSignCsrBadRequest().WithPayload(&models.Error{Code: &code, Message: &message})
	}

	serial, serialErr := rand.Int(rand.Reader, big.NewInt(20000))
	if nil != serialErr {
		log.Fatalf("serialErr: %v\n", serialErr.Error())
	}


	template := x509.Certificate{

		SignatureAlgorithm: x509.SHA512WithRSA,
		SerialNumber: serial,

		Subject: pkix.Name{
			CommonName:   csr.Subject.CommonName,
			Organization: csr.Subject.Organization,
		},

		DNSNames:           csr.DNSNames,
		IPAddresses:        csr.IPAddresses,

		NotBefore: time.Now(),
		NotAfter: time.Now().Add(time.Hour * 24 * 30),

		PublicKey: csr.PublicKey,
		PublicKeyAlgorithm: csr.PublicKeyAlgorithm,

		Version: 1,
	}




	ctx, err := crypto11.ConfigureFromFile("config")
	if nil != err {
		log.Fatalf("err: %v\n", err.Error())
	}
	defer func() {
		ctx.Close()
	}()

	caPrivKey := FindCaSigningKey(ctx)

    createdCert, certCreationErr := x509.CreateCertificate(rand.Reader, &template, globalCert, csr.PublicKey, caPrivKey)
    if nil != certCreationErr {
    	log.Printf("certCreationErr: %v\n", certCreationErr.Error())
	}

	createdCertB64 := base64.StdEncoding.EncodeToString(createdCert)

	return operations.NewPostSignCsrOK().WithPayload(&models.Certificate{CertificateBodyB64: &createdCertB64})
}
