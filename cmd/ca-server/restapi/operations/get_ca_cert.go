// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// GetCaCertHandlerFunc turns a function with the right signature into a get ca cert handler
type GetCaCertHandlerFunc func(GetCaCertParams) middleware.Responder

// Handle executing the request and returning a response
func (fn GetCaCertHandlerFunc) Handle(params GetCaCertParams) middleware.Responder {
	return fn(params)
}

// GetCaCertHandler interface for that can handle valid get ca cert params
type GetCaCertHandler interface {
	Handle(GetCaCertParams) middleware.Responder
}

// NewGetCaCert creates a new http.Handler for the get ca cert operation
func NewGetCaCert(ctx *middleware.Context, handler GetCaCertHandler) *GetCaCert {
	return &GetCaCert{Context: ctx, Handler: handler}
}

/*GetCaCert swagger:route GET /ca_cert getCaCert

Get the CA cert

*/
type GetCaCert struct {
	Context *middleware.Context
	Handler GetCaCertHandler
}

func (o *GetCaCert) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewGetCaCertParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
