// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/base64"
	"github.com/ThalesIgnite/crypto11"
	ca_server "gitlab.com/robfitzpatrick/est/cmd/ca-server"
	"log"
	"math/big"
	"net/http"
	"os"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"

	"gitlab.com/robfitzpatrick/est/cmd/ca-server/restapi/operations"
)

//go:generate swagger generate server --target ../../ca-server --name ESTCAServer --spec ../est-ca.yml --principal interface{}

func configureFlags(api *operations.ESTCAServerAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.ESTCAServerAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.GetCaCertHandler = operations.GetCaCertHandlerFunc(ca_server.GetCaCertHandler)

	api.PostSignCsrHandler = operations.PostSignCsrHandlerFunc(ca_server.PostSignCsrHandler)

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {

	const ca_cert_save_filename = "ca_cert.save"

	serial, err := rand.Int(rand.Reader, big.NewInt(20000))
	if nil != err {
		log.Fatalf("err: %v\n", err.Error())
	}

	ctx, err := crypto11.ConfigureFromFile("config")
	if nil != err {
		log.Fatalf("err: %v\n", err.Error())
	}
	defer func() {
		ctx.Close()
	}()

	foundKey := ca_server.FindCaSigningKey(ctx)

	if nil == foundKey {

		key := ca_server.GenerateCaSigningKey(ctx)
		if nil == key {
			log.Fatalf("key was nil\n")
		}

		ca := &x509.Certificate{
			Subject: pkix.Name{
				CommonName: "PiCam CA",
			},
			SerialNumber:          serial,
			NotAfter:              time.Now().Add(365 * 24 * time.Hour),
			IsCA:                  true,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
			KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
			BasicConstraintsValid: true,
		}

		csr := key.Public()
		certBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, csr, key)
		if nil != err {
			log.Fatalf("err: %v\n", err.Error())
		}

		ca_server.SetGlobalCert(certBytes)

		ca_cert_save, ca_cert_save_err := os.Create(ca_cert_save_filename)
		if nil != ca_cert_save_err {
			log.Fatalf("ca_cert_save_err: %v\n", ca_cert_save_err.Error())
		}
		defer ca_cert_save.Close()

		base64Bytes := base64.StdEncoding.EncodeToString(certBytes)

		numBytesWritten, writeErr := ca_cert_save.WriteString(base64Bytes)
		if nil != writeErr {
			log.Fatalf("writeErr: %v\n", writeErr.Error())
		}
		if numBytesWritten != len(base64Bytes) {
			log.Fatalf("Bad write length\n")
		}

	} else {

		// Restore and print here?
		certFile, certFileErr := os.Open(ca_cert_save_filename)
		if nil != certFileErr {
			log.Fatalf("certFileErr: %v\n", certFileErr.Error())
		}
		defer func() {
			if nil != certFile.Close() {
				log.Fatalf("Failed to close certFile\n")
			}
		}()

		certFileInfo, certFileInfoErr := certFile.Stat()
		if nil != certFileInfoErr {
			log.Fatalf("certFileInfoErr: %v\n", certFileInfoErr.Error())
		}

		certBytes := make([]byte, certFileInfo.Size())
		readBytes, readError := certFile.Read(certBytes)
		if nil != readError {
			log.Fatalf("readError: %v\n", readError.Error())
		}
		if int64(readBytes) != certFileInfo.Size() {
			log.Fatalf("Weird read size\n")
		}

		decodedCertBytes, decodeErr := base64.StdEncoding.DecodeString(string(certBytes))
		if nil != decodeErr {
			log.Fatalf("decodeErr: %v\n", decodeErr.Error())
		}

		ca_server.SetGlobalCert(decodedCertBytes)

	}

}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
