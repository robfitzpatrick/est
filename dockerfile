from golang:latest

RUN apt-get update && apt-get install -y softhsm2

RUN softhsm2-util --init-token --slot 0 --label default --so-pin changeme --pin changeme
